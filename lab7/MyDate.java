
public class MyDate {
	
	private int year;
	private int month;
	private int day;

	int[] max_days = {31,28,31,30,31,30,31,31,30,31,30,31};

	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month-1;
		this.year = year;
	}

	public void incrementDay() {
		this.day++;
		
		if(day>max_days[month]) {
			day = 1;
			incrementMonth();
		}
		
	}

	public void incrementYear(int i) {
		this.year++;
		
	}

	public void decrementDay(int i) {
		
		this.day--;
		
		if(day==0) {
			decrementMonth();
			
			if(month==1)
				day=29;
			else {
				day = max_days[month];
			}
		}
		
		day = max_days[month];
		
	}

	public void decrementYear() {
		// TODO Auto-generated method stub
		
	}

	public void decrementMonth() {

	}

	public void incrementDay(int i) {
		// TODO Auto-generated method stub
		
	}

	public void decrementMonth(int i) {
		
		incrementMonth(-i);
		
	}

	public void incrementMonth(int i) {
		int newMonth = (month + i) % 12;
		int yearDiff = 0;
		
		if(newMonth<0) {
			newMonth+= 12;
			yearDiff = -1;
		}
		
		yearDiff = (month + i) / 12;
		month = newMonth;
		incrementYear(yearDiff);
	}

	public void decrementYear(int i) {
		// TODO Auto-generated method stub
		
	}

	public void incrementYear() {
		// TODO Auto-generated method stub
		
	}

	public void incrementMonth() {
		// TODO Auto-generated method stub
		
	}
	
	public void inLeapYear() {
		
	}

	public boolean isBefore(MyDate anotherDate) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isAfter(MyDate anotherDate) {
		// TODO Auto-generated method stub
		return false;
	}

	public int dayDifference(MyDate anotherDate) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void decrementDay() {
		// TODO Auto-generated method stub
		
	}
	
	public String toString() {
		
		return year + "-" + month + "-" + day;
	}
	

}
