package shapes3d;

public class Cyclinder {

	int height;
	
	public Cyclinder(int radius, int height) {
		this.height = height;
	}
	
	public double area() {
		return 2 * 2*Math.PI*height * height;
	}
	
	public String toString() {
		return "Cyclinder";
	}
	
}
