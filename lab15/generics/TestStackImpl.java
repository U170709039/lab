package generics;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestStackImpl {

	public static void main(String[] args) {
		
		List<Integer> lst = new LinkedList<>();
		lst.add(5);
		lst.add(6);	
		
		/**
		for(int i=0; i<lst.size(); i++) {
			System.out.println(lst.get(i));
		}
		**/
		
		List<String> lstStr = new LinkedList<>();
		lstStr.add("A");
		lstStr.add("B");
		
		
		List<Object> lstObj = new ArrayList<>();
		lstObj.add(lstStr);
		lstObj.add(lst);
		
		Stack<Object> stackObj = new StackImpl<>();
		
		Stack<Object> stack = new StackImpl<>();
		stack.push("A");
		stack.push("B");
		stack.push("C");
		stack.push("D");
		
		stackObj.addAll(stack);
		
		Stack<String> stack2 = new StackImpl<>();
		stack2.push("A");
		stack2.push("B");
		stack2.push("C");
		stack2.push("D");
		
		while (!stack2.empty()){
			System.out.println(stack2.pop());
		}
		
	}
}