package generics;

import java.util.List;

public interface Stack<T> {
	
	void push(T obj);
	
	T pop();
	
	boolean empty();

	void addAll(Stack<? extends T> stack);
	
	List<T> toList();

}