import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);

		int moveCount = 0;
		int currentPlayer = 0;

		while(moveCount<9){
	
		System.out.print("Player "+ currentPlayer + " enter row number:");
		int row = reader.nextInt();
		System.out.print("Player "+ currentPlayer + " enter column number:");
		int col = reader.nextInt();

		if((row>0 && row<=3) && (col>0 && col<=3)){
		
		board[row - 1][col - 1] = ((currentPlayer+1)%2==0) ?  'X' : '0';

		printBoard(board);
		
		moveCount++;		

		
			if(checkBoard(board, (row-1), (col-1))){
				System.out.println("Winner: Player"+currentPlayer);
				break;
			}


		currentPlayer = (moveCount%2==0) ? 0 : 1;


		}else{
			
			System.out.println("The numbers was invalid.");
			
		}

		

		}
	}

	public static boolean checkBoard(char[][] board, int rowLast, int colLast) {

		char symbol = board[rowLast][colLast];

		boolean win = true;

		/* check diagonal, from top left to bottom right */
		if(rowLast==colLast){

			for( int location=0; location < 3; location++){

				win = true;

				if(board[location][location] != symbol){

					win = false;
					break;

				}

			}

			if(win){
				return true;
			}

		} 



		if(rowLast+colLast==2){


			for( int row=0; row <3; row++){
				

				win = true;

				if(board[row][2-row] != symbol){

					win = false;
					break;

				}

			}

			if(win){
				return true;
			}

		}

		/* Check columns */
		for(int col=0; col<3; col++){

			if(board[rowLast][col] != symbol){
				win = false;
				break;
			}

		}

		if (win) {
			return true;
		}


		win = true;
	
		/* Check rows */
		for(int row=0; row<3; row++){

			if(board[row][colLast] != symbol){


				win = false;
				break;

			}

		}

		if(win) {
			return true;
		}


		return false;

	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}
