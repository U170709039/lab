

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		

		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it:");		

		int guess = reader.nextInt();	

		int count = 1;
	
		while ( guess!= number && guess!=-1 ){

			if(guess < number){
				System.out.println("Mine is greater than you guess");
			}else{
				System.out.println("Mine is less than your guess");
			}
			
			guess = reader.nextInt();

			count++;
		}
		
	
		
		if ( number == guess) {
			System.out.println("Congratulations! You won after " + count + " attemps");
		}else{
			System.out.println("Sorry! THe number was "+number );
		}
		
		reader.close(); //Close the resource before exiting
	}
	
	
}
