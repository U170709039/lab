public class FindMin {
	public static void main(String[] args){

		int value1 = Integer.parseInt(args[0]);
		int value2 = Integer.parseInt(args[1]);
		int value3 = Integer.parseInt(args[2]);

		int min, result;

		min = (value1>value2) ? value2 : value1;
		result = (min>value3) ? value3 : min;

		System.out.println(result);

	}
}
