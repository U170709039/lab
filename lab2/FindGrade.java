public class FindGrade {
	public static void main(String[] args){

		int score = Integer.parseInt(args[0]);
		char result='N';

		if(score==0 || score<0 || score>100){
			System.out.println("Score is invalid");
			return;
		}

		if (score >= 90) {
			result = 'A';
		} else if(score >= 80) {
			result = 'B';
		} else if(score >= 70){
			result = 'C';
		} else if(score >= 60){
			result = 'D';
		} else if(score >= 0){
			result = 'F';
		}

		System.out.println("Your Grade is= " + result );
	}
}
